package com.mobileread.ixtab.collman;

import java.util.TimerTask;

import javax.swing.JLabel;
import javax.swing.SwingUtilities;

public class Status {
	public static final JLabel label = new JLabel("");

	private static TimerTask cleanupTask = null;
	
	public static final int DEFAULT_TIMEOUT_MS = 5000;
	public static final int FOREVER = 0;

	public static synchronized void set(String status, final int timeout) {
		if (cleanupTask != null) {
			cleanupTask.cancel();
			cleanupTask = null;
		}

		final String display = status == null || status.trim().equals("") ? null
				: status.trim();
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				if (display == null) {
					label.setVisible(false);
				} else {
					label.setVisible(true);
					label.setText(" \u25B6 " + display);
				}
			}
		});

		if (timeout != 0) {
			cleanupTask = new TimerTask() {
				public void run() {
					set(null,0);
				}
			};
			GlobalTimer.get().schedule(cleanupTask, timeout);
		}
	}
}
