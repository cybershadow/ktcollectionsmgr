package com.mobileread.ixtab.collman;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import com.amazon.kindle.content.catalog.CatalogEntry;
import com.mobileread.ixtab.collman.adapters.Predicate;
import com.mobileread.ixtab.collman.adapters.PredicateFactory;
import com.mobileread.ixtab.collman.adapters.fw512.SearchHandler;
import com.mobileread.ixtab.collman.catalog.Catalog;
import com.mobileread.ixtab.collman.catalog.Collection;
import com.mobileread.ixtab.collman.catalog.Entry;
import com.mobileread.ixtab.collman.catalog.RootCollection;

public class Model implements ActionListener {
	private static final int DEFAULT_ENTRIES_PER_PAGE = 5;
	private static final int MAXED_ENTRIES_PER_PAGE = 11;

	private static final Predicate PRED_IS_VISIBLE = PredicateFactory
			.isTrue("isVisibleInHome");

	private static final Predicate PRED_IS_COLLECTION = PredicateFactory
			.equals("type", "Collection");

	private static final Predicate PRED_IS_SPECIALITEM = PredicateFactory
			.startsWith("cdeType", "Entry:Item:");

	/*
	 * looks complicated, is simple: no collections, no special items (like
	 * Archive, Images, etc.), nothing that is invisible, and nothing that is
	 * contained in more than 0 collections.
	 */
	private static final Predicate PRED_IS_UNFILED = PredicateFactory
			.not(PredicateFactory.or(new Predicate[] {
					PRED_IS_COLLECTION,
					PRED_IS_SPECIALITEM,
					PredicateFactory.not(PRED_IS_VISIBLE),
					PredicateFactory.and(new Predicate[] {
							PredicateFactory.notNull("collectionCount"),
							PredicateFactory.greater("collectionCount", 0,
									false), }) }));

	private static final Model[] models = new Model[] {
			new Model(PanelPosition.UPPER), new Model(PanelPosition.LOWER) };

	private boolean collectionsOnly = Settings.get().isShowCollectionsOnly();
	private boolean visibleOnly = Settings.get().isHideInvisibleInHome();
	private boolean filterLowerOnly = Settings.get().isFilterLowerPanelOnly();
	private boolean unfiledOnly = Settings.get().isShowUnfiledItemsOnly();

	private Predicate searchFilter = null;

	public static Model get(int position) {
		switch (position) {
		case PanelPosition.UPPER:
			return models[0];
		case PanelPosition.LOWER:
			return models[1];
		}
		return null;
	}

	private final List path = new ArrayList();
	private int offset = 0;

	private final int position;
	private int entriesPerPage = DEFAULT_ENTRIES_PER_PAGE;

	private Model(int position) {
		this.position = position;
		path.add(Entry.getRoot());
		Event.addListener(this);
	}

	public void actionPerformed(ActionEvent event) {
		Event e = (Event) event;
		if (!e.appliesTo(position)) {
			return;
		}
		switch (e.getID()) {
		case Event.LOWER_PANEL_DISABLE:
		case Event.LOWER_PANEL_ENABLE:
			handleLowerPanelVisibility(e.getID());
			break;
		case Event.VIEW_ENTRY_CLICKED:
			handleViewEntryClicked((Collection) e.getSource());
			break;
		case Event.PATH_ENTRY_CLICKED:
			handlePathEntryClicked((Collection) e.getSource());
			break;
		case Event.ENTRY_CHANGED:
			handleEntryChanged((Entry) e.getSource());
			break;
		case Event.COLLECTION_DELETED:
			handleCollectionDeleted((Collection) e.getSource());
			break;
		case Event.SET_PATH:
			handleSetPath((List)e.getSource());
			break;
		case Event.VIEW_CHANGED:
		case Event.STARTUP:
			postReloadEvent(false);
			break;
		case Event.COMMAND_LEFT:
			handleLeftEvent();
			break;
		case Event.COMMAND_RIGHT:
			handleRightEvent();
			break;
		case Event.SEARCH:
			handleSearchEvent();
			break;
		case Event.DISPLAY_COLLECTIONS_ONLY:
			collectionsOnly = true;
			postReloadEvent(true);
			break;
		case Event.DISPLAY_ALL:
			collectionsOnly = false;
			postReloadEvent(true);
			break;
		case Event.INVISIBLE_HIDE:
			visibleOnly = true;
			postReloadEvent(true);
			break;
		case Event.INVISIBLE_SHOW:
			visibleOnly = false;
			postReloadEvent(true);
			break;
		case Event.FILTER_LOWER_ONLY_ON:
			filterLowerOnly = true;
			postReloadEvent(true);
			break;
		case Event.FILTER_LOWER_ONLY_OFF:
			filterLowerOnly = false;
			postReloadEvent(true);
			break;
		case Event.SHOW_UNFILED_ONLY_ON:
			unfiledOnly = true;
			postReloadEvent(true);
			break;
		case Event.SHOW_UNFILED_ONLY_OFF:
			unfiledOnly = false;
			postReloadEvent(true);
			break;
		case Event.SORT_CHANGED:
			postReloadEvent(true);
			break;
		case Event.RELOADED:
			postDirectionEvents();
			break;
		}
	}

	private void handleSearchEvent() {
		String search = SearchHandler.getInstance().getSearchQuery();
		if (position != PanelPosition.UPPER) {
			return;
		}
		if (search.length() == 0) {
			searchFilter = null;
		} else {
			searchFilter = PredicateFactory.inList("uuid", Catalog.getCache()
					.find(search));
		}
		// Status.set("search");
		postReloadEvent(true);
	}

	private void handleCollectionDeleted(Collection source) {
		if (Entry.equals(source, getCurrentCollection())) {
			path.remove(path.size() - 1);
			postReloadEvent(true);
		}
	}

	private void handleLeftEvent() {
		if (canGoLeft()) {
			offset = Math.max(0, offset - getEntriesPerPage());
			postReloadEvent(false);
		}
	}

	private void handleRightEvent() {
		if (canGoRight()) {
			offset += getEntriesPerPage();
			postReloadEvent(false);
		}
	}

	private void handleLowerPanelVisibility(int event) {
		if (position == PanelPosition.UPPER) {
			entriesPerPage = event == Event.LOWER_PANEL_ENABLE ? DEFAULT_ENTRIES_PER_PAGE
					: MAXED_ENTRIES_PER_PAGE;
			postReloadEvent(false);
		}
	}

	private void handleEntryChanged(Entry source) {
		int match = -2; // path.size() should never be 0, but still...
		// update all matching entries in path
		for (int i = 0; i < path.size(); ++i) {
			if (Entry.equals(source, path.get(i))) {
				Entry.copyBackend(path.get(i), source);
				match = i;
			}
		}
		// if last match is current collection, reload it
		if (match == path.size() - 1) {
			postReloadEvent(true);
		}
	}

	private void handleViewEntryClicked(Collection source) {
		path.add(source);
		postReloadEvent(true);
	}
	
	public List getPathUuids() {
		List uuids = new ArrayList(path.size());
		for (int i= 0; i < path.size(); ++i) {
			uuids.add(((Collection)path.get(i)).getUuid());
		}
		return uuids;
	}
	
	private void handleSetPath(List pathUuids) {
		if (pathUuids.size() < 1 || pathUuids.get(0) != RootCollection.getRootUuid()) {
			return;
		}
		boolean samePath = true;
		if (path.size() != pathUuids.size()) {
			samePath = false;
		} else {
			for (int i=0; i < path.size(); ++i) {
				Collection c = (Collection) path.get(i);
				// just to be on the safe side, we use String comparison for UUIDS
				if (!c.getUuid().toString().equalsIgnoreCase(pathUuids.get(i).toString())) {
					samePath = false;
					break;
				}
			}
		}
		if (samePath) {
			return;
		}
		// traverse to root collection
		while (path.size() > 1) {
			path.remove(path.size()-1);
		}
		// add rest of collections to path
		for (int i=1; i < pathUuids.size(); ++i) {
			Object uuid = pathUuids.get(i);
			CatalogEntry loaded = Catalog.load(uuid);
			Object coll = Entry.instantiate(loaded);
			path.add((Collection) coll);
			//path.add((Collection) Entry.instantiate(Catalog.load(pathUuids.get(i))));
		}
		postReloadEvent(true);
	}

	private void handlePathEntryClicked(Collection source) {
		int index = path.indexOf(source);
		if (index == -1) {
			// not sure if needed, but better safe than sorry.
			return;
		}
		boolean changed = false;
		while (index < path.size() - 1) {
			changed = true;
			path.remove(path.size() - 1);
		}
		if (changed) {
			postReloadEvent(true);
		}
	}

	private void postReloadEvent(boolean resetOffset) {
		if (resetOffset) {
			offset = 0;
		}
		post(Event.RELOAD);
	}

	private void post(int event) {
		Event.post(event, this, position);
	}

	private void postDirectionEvents() {
		if (canGoLeft()) {
			post(Event.ENABLE_LEFT);
		} else {
			post(Event.DISABLE_LEFT);
		}
		if (canGoRight()) {
			post(Event.ENABLE_RIGHT);
		} else {
			post(Event.DISABLE_RIGHT);
		}
	}

	private boolean canGoLeft() {
		return offset > 0;
	}

	private boolean canGoRight() {
		return offset + getEntriesPerPage() < getCurrentCollection().getSize();
	}

	public Collection getCurrentCollection() {
		return (Collection) path.get(path.size() - 1);
	}

	public int getOffset() {
		return offset;
	}

	public int getEntriesPerPage() {
		return entriesPerPage;
	}

	public int getDefaultEntriesPerPage() {
		return DEFAULT_ENTRIES_PER_PAGE;
	}

	public int getMaxedEntriesPerPage() {
		return position == PanelPosition.UPPER ? MAXED_ENTRIES_PER_PAGE
				: DEFAULT_ENTRIES_PER_PAGE;
	}

	public Predicate getFilter() {
		Predicate visible = visibleOnly && getCurrentCollection().isRoot() ? PRED_IS_VISIBLE
				: null;
		Predicate collections = collectionsOnly ? PRED_IS_COLLECTION : null;
		if (filterLowerOnly && position == PanelPosition.UPPER) {
			visible = null;
			collections = null;
		}
		Predicate unfiled = null;
		if (position == PanelPosition.UPPER && unfiledOnly) {
			unfiled = PRED_IS_UNFILED;
		}
		return combinePredicates(new Predicate[] { searchFilter, visible,
				collections, unfiled });
	}

	private Predicate combinePredicates(Predicate[] p) {
		List nonNull = new ArrayList();
		for (int i = 0; i < p.length; ++i) {
			if (p[i] != null) {
				nonNull.add(p[i]);
			}
		}
		if (nonNull.isEmpty()) {
			return null;
		} else if (nonNull.size() == 1) {
			return (Predicate) nonNull.get(0);
		}
		Predicate[] result = new Predicate[nonNull.size()];
		for (int i = 0; i < result.length; ++i) {
			result[i] = (Predicate) nonNull.get(i);
		}
		return PredicateFactory.and(result);
	}

	public List getPath() {
		return path;
	}
}
