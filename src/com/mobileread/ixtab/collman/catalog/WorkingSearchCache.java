package com.mobileread.ixtab.collman.catalog;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.amazon.kindle.content.catalog.CatalogEntry;
import com.mobileread.ixtab.collman.adapters.CatalogAdapter;

public class WorkingSearchCache implements SearchCache {

	private final List entries = new ArrayList();
	
	public WorkingSearchCache() {
	}
	
	public void init(CatalogEntry[] catalog) {
		if (catalog != null) {
			for (int i=0; i < catalog.length; ++i) {
				try {
					Object uuid = CatalogAdapter.INSTANCE.getUUID(catalog[i]);
					String title = CatalogAdapter.INSTANCE.getTitle(catalog[i]).trim().toLowerCase();
					String credits = null;
					try {
						credits = com.mobileread.ixtab.collman.catalog.Entry.getAuthorFromCatalog(catalog[i]).trim().toLowerCase();
					} catch (Throwable t) {}
					entries.add(new Entry(uuid, title, credits));
				} catch (Throwable unexpected) {}
			}
			Collections.sort(entries);
		}
	}
	
	public void delete(Object uuid) {
		Entry delete = new Entry(uuid, "", null);
		synchronized (entries) {
			int index = Collections.binarySearch(entries, delete);
			if (index >= 0) {
				entries.remove(index);
			}
		}
	}

	public void set(Object uuid, String newTitle) {
		Entry replace = new Entry(uuid, newTitle, null);
		synchronized (entries) {
			int index = Collections.binarySearch(entries, replace);
			if (index < 0) {
				index = -(index+1);
				entries.add(index, replace);
			} else {
				entries.set(index, replace);
			}
		}
	}
	
	
	public Object[] find(String search) {
		search = search.toLowerCase();
		List resultList = new ArrayList();
		synchronized (entries) {
			// going backwards to avoid invoking size() repeatedly
			for (int i=entries.size()-1; i>= 0; --i) {
				Entry candidate = (Entry) entries.get(i);
				if (candidate.matches(search)) {
					resultList.add(candidate.uuid);
				}
			}
		}
		Object[] result = new Object[resultList.size()];
		for (int i=0; i < result.length; ++i) {
			result[i] = resultList.get(i);
		}
		return result;
	}
	
	private static class Entry implements Comparable {
		private final Object uuid;
		private final String title;
		private final String credits;
		
		public Entry(Object uuid, String title, String credits) {
			this.uuid = uuid;
			this.title = title;
			this.credits = credits;
		}

		public boolean matches(String search) {
			if (title.indexOf(search) >= 0) {
				return true;
			}
			return credits == null ? false : credits.indexOf(search) >= 0;
		}

		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((uuid == null) ? 0 : uuid.hashCode());
			return result;
		}

		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Entry other = (Entry) obj;
			if (uuid == null) {
				if (other.uuid != null)
					return false;
			} else if (!uuid.equals(other.uuid))
				return false;
			return true;
		}

		public int compareTo(Object o) {
			return toString().compareTo(o.toString());
		}
		
		public String toString() {
			return uuid.toString();
		}
	}

}
