package com.mobileread.ixtab.collman.catalog;

import java.util.ArrayList;
import java.util.List;

import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import com.amazon.kindle.content.catalog.CatalogEntry;
import com.amazon.kindle.content.catalog.CatalogEntryCollection;
import com.amazon.kindle.content.catalog.CatalogItem;
import com.amazon.kindle.content.catalog.MutableEntry;
import com.mobileread.ixtab.collman.Event;
import com.mobileread.ixtab.collman.PanelPosition;
import com.mobileread.ixtab.collman.adapters.CatalogAdapter;

public abstract class Entry {

	protected Entry() {
	}

	public static String getAuthorFromCatalog(CatalogEntry entry) {
		try {
			String credits = CatalogAdapter.INSTANCE.getFirstCreditAsJSON(entry);
			if (credits == null) {
				return null;
			}
			JSONObject o = (JSONObject) JSONValue.parse(credits);
			o = (JSONObject) o.get("name");
			return (String) o.get("display");
		} catch (Throwable t) {
			return null;
		}
	}
	
	public static Entry instantiate(CatalogEntry catalogEntry) {
		if (catalogEntry instanceof CatalogEntryCollection) {
			return new EntryCollection((CatalogEntryCollection)catalogEntry);
		} else if (catalogEntry instanceof CatalogItem) {
			return new Item((CatalogItem)catalogEntry);
		}
		throw new RuntimeException("cannot instantiate: "+catalogEntry);
	}

	public abstract CatalogEntry getBackend();
	protected abstract void setBackend(CatalogEntry update);

	protected abstract boolean isCollection();
	
	public static final RootCollection getRoot() {
		return RootCollection.getInstance();
	}
	
	public final boolean isRoot() {
		return equals(this, getRoot());
	}
	
	public String toString() {
		return " " + getName();
	}
	
	public String getAuthor() {
		return getAuthorFromCatalog(getBackend());
	}

	public Object getUuid() {
		return CatalogAdapter.INSTANCE.getUUID(getBackend());
	}
	
	public boolean isVisible() {
		return CatalogAdapter.INSTANCE.isVisibleInHome(getBackend());
	}

	public void setVisible(boolean visible) {
		MutableEntry mutable = Catalog.createMutableEntry(this);
		CatalogAdapter.INSTANCE.setIsVisibleInHome(mutable, visible);
		Catalog.update(mutable);
		reload();
	}

	public void reload() {
		if (!isRoot()) {
			setBackend(Catalog.load(getUuid()));
		}
		Event.post(Event.ENTRY_CHANGED, this, PanelPosition.BOTH);
	}

	public static boolean equals(Object o1, Object o2) {
		if (o1 == null || o2 == null || !(o1 instanceof Entry && o2 instanceof Entry)) {
			return false;
		}
		Entry e1 = (Entry)o1;
		Entry e2 = (Entry)o2;
		return (e1.getUuid().equals(e2.getUuid()));
	}

	public static void copyBackend(Object targetEntry, Object sourceEntry) {
		Entry target = (Entry)targetEntry;
		Entry source = (Entry)sourceEntry;
		target.setBackend(source.getBackend());
	}
	
	public String getName() {
		return CatalogAdapter.INSTANCE.getTitle(getBackend());
	}

	public int countParents() {
		if (!CatalogAdapter.INSTANCE.hasCloudCollections()) {
			return CatalogAdapter.INSTANCE.countParents(getBackend());
		} else {
			return getParents().length;
		}
	}
	
	public Collection[] getParents() {
		Object[] uuids = CatalogAdapter.INSTANCE.getParents(getBackend());
		if (uuids == null || uuids.length == 0) {
			return new Collection[0];
		}
		
		List parents = new ArrayList(uuids.length);
		for (int i=0; i < uuids.length; ++i) {
			CatalogEntry entry = Catalog.load(uuids[i]);
			if (entry != null) {
				parents.add(Entry.instantiate(entry));
			}
		}
		Collection[] collections = new Collection[parents.size()];
		parents.toArray(collections);
		return collections;
	}
	
}
