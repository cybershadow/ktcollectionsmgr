package com.mobileread.ixtab.collman.catalog;

import com.amazon.kindle.content.catalog.CatalogEntry;

public class RootCollection extends Collection {

	private static final Object uuid = Catalog.createRandomUUID();
//	private static final RootCollection instance = new RootCollection();
	
	private RootCollection() {}
	
	public static Object getRootUuid() {
		return uuid;
	}
	
	public static RootCollection getInstance() {
		return new RootCollection();
	}
	
	public CatalogEntry getBackend() {
		return null;
	}
	
	protected void setBackend(CatalogEntry backend) {
	}

	public boolean isVisible() {
		return true;
	}
	
	public Object getUuid() {
		return uuid;
	}

	public String getName() {
		return "/";
	}

	public String toString() {
		return getName();
	}
}
