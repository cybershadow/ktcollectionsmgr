package com.mobileread.ixtab.collman.catalog;

import com.amazon.kindle.content.catalog.CatalogEntry;
import com.amazon.kindle.content.catalog.CatalogItem;

public class Item extends Entry {

	protected CatalogItem backend;
	
	public Item(CatalogItem catalogEntry) {
		this.backend = catalogEntry;
	}

	public CatalogEntry getBackend() {
		return backend;
	}

	protected boolean isCollection() {
		return false;
	}

	protected void setBackend(CatalogEntry update) {
		backend = (CatalogItem) update;
	}

}
