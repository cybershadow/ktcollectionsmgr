package com.mobileread.ixtab.collman.messages;

import com.mobileread.ixtab.collman.Event;
import com.mobileread.ixtab.collman.I18n;
import com.mobileread.ixtab.collman.PanelPosition;
import com.mobileread.ixtab.collman.Status;

public class ActionCompletedMessage {
	private static final String message = I18n.get().i18n(I18n.ACTION_DONE_KEY, I18n.ACTION_DONE_VALUE);

	public static void show() {
		Status.set(message, Status.DEFAULT_TIMEOUT_MS);
		Event.post(Event.STARTUP, ActionCompletedMessage.class, PanelPosition.BOTH);
	}
}
