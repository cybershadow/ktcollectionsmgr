package com.mobileread.ixtab.collman;

public class PanelPosition {
	public static final int UPPER = 1;
	public static final int LOWER = 2;
	public static final int BOTH = UPPER | LOWER;
}
