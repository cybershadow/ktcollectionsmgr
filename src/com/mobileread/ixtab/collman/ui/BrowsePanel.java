package com.mobileread.ixtab.collman.ui;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import com.mobileread.ixtab.collman.Event;
import com.mobileread.ixtab.collman.Model;
import com.mobileread.ixtab.collman.PanelPosition;
import com.mobileread.ixtab.collman.catalog.Collection;
import com.mobileread.ixtab.collman.catalog.Entry;
import com.mobileread.ixtab.collman.ui.model.ListBackedComboboxModel;

public class BrowsePanel extends JPanel implements ActionListener {
	private static final long serialVersionUID = 1L;

	private final JButton previousButton = new JButton("\u25C0");
	private final JButton nextButton = new JButton("\u25B6");
	private final EntriesPanel panel;
	private final ListBackedComboboxModel path;
	private final PathPanel pathPanel;
	private final int position;
	private final Model model;

	public BrowsePanel(int position) {
		this.position = position;
		this.model = Model.get(position);
		this.path = new ListBackedComboboxModel(model.getPath());
		panel = new EntriesPanel(position);
		pathPanel = new PathPanel(path, position);
		Event.addListener(this);
		setLayout(new BorderLayout(5, 5));
		setBorder(Borders.GRAY2WHITE5);
		add(panel, BorderLayout.CENTER);

		JPanel buttonsPanel = new JPanel(new GridLayout(1, 2, 5, 0));
		buttonsPanel.add(previousButton);
		buttonsPanel.add(nextButton);
		addAction(previousButton, Event.COMMAND_LEFT);
		addAction(nextButton, Event.COMMAND_RIGHT);

		JPanel northPanel = new JPanel(new BorderLayout(5, 5));
		northPanel.add(pathPanel, BorderLayout.CENTER);
		northPanel.add(buttonsPanel, BorderLayout.EAST);

		add(northPanel, BorderLayout.NORTH);
		setCollection(model.getCurrentCollection());
	}

	private void addAction(JButton button, final int event) {
		button.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				Event.post(event, BrowsePanel.this, position);
			}
		});
	}

	private void setCollection(final Collection collection) {
		final Entry[] entries = collection.getEntries(position, model
				.getFilter(), model.getOffset(), model.getEntriesPerPage());
		path.syncToBackend();
		Event.post(Event.RELOADED, collection, position);

		SwingUtilities.invokeLater(new Runnable() {

			public void run() {
				panel.setEntries(entries);
				path.setSelectedItem(path.getElementAt(path.getSize() - 1));
			}
		});
	}

	public void actionPerformed(ActionEvent event) {
		Event e = (Event) event;
		if (!e.appliesTo(position)) {
			return;
		}
		if (e.getID() == Event.LOWER_PANEL_DISABLE
				|| e.getID() == Event.LOWER_PANEL_ENABLE) {
			if (position == PanelPosition.LOWER) {
				if (e.getID() == Event.LOWER_PANEL_DISABLE) {
					panel.setEntries(new Entry[model.getEntriesPerPage()]);
					setVisible(false);
				} else {
					setCollection(model.getCurrentCollection());
					setVisible(true);
				}
			}
			return;
		}
		if (e.getID() == Event.RELOAD) {
			setCollection(model.getCurrentCollection());
		}
		if (e.getID() == Event.ENABLE_LEFT) {
			previousButton.setEnabled(true);
		}
		if (e.getID() == Event.DISABLE_LEFT) {
			previousButton.setEnabled(false);
		}
		if (e.getID() == Event.ENABLE_RIGHT) {
			nextButton.setEnabled(true);
		}
		if (e.getID() == Event.DISABLE_RIGHT) {
			nextButton.setEnabled(false);
		}
	}
}
