package com.mobileread.ixtab.collman.ui.menu.sort;

import com.amazon.kindle.kindlet.ui.KMenuItem;
import com.mobileread.ixtab.collman.Event;
import com.mobileread.ixtab.collman.I18n;
import com.mobileread.ixtab.collman.PanelPosition;
import com.mobileread.ixtab.collman.Settings;
import com.mobileread.ixtab.collman.adapters.CatalogService;
import com.mobileread.ixtab.collman.ui.menu.VirtualMenu;

public class SortOptionsMenu extends VirtualMenu {

	private final int panelPosition;
	private final int minEvent;
	private final int maxEvent;

	public SortOptionsMenu(VirtualMenu parent, int panelPosition) {
		super(parent);
		this.panelPosition = panelPosition;
		minEvent = panelPosition == PanelPosition.LOWER ? Event.SORT_ID_LOWER_MIN : Event.SORT_ID_UPPER_MIN;
		maxEvent = panelPosition == PanelPosition.LOWER ? Event.SORT_ID_LOWER_MAX : Event.SORT_ID_UPPER_MAX;
	}

	public String getLabel() {
		String label = I18n.get().i18n(I18n.MENU_SORTOPTIONS_KEY,
				I18n.MENU_SORTOPTIONS_VALUE);
		if (panelPosition != PanelPosition.BOTH) {
			label += " : ";
			if (panelPosition == PanelPosition.UPPER) {
				label += I18n.get().i18n(I18n.PANEL_UPPER_KEY,
						I18n.PANEL_UPPER_VALUE);
			} else {
				label += I18n.get().i18n(I18n.PANEL_LOWER_KEY,
						I18n.PANEL_LOWER_VALUE);
			}
		}
		return label;
	}

	protected KMenuItem[] getItems() {
		KMenuItem[] items = new KMenuItem[7];
		int i=0;
		items[i++] = getParentItem();
		i = addSortItems(items, i);
		items[i++] = new KMenuItem(new ToggleIndependentSortButton(this.parent));
		return items;
	}

	private int addSortItems(KMenuItem[] items, int i) {
		int e=0;
		items[i++] = new KMenuItem(new SortButton(I18n.get().i18n(
				I18n.SORT_TITLE_KEY, I18n.SORT_TITLE_VALUE),
				CatalogService.Sort.TITLE, minEvent+e++, panelPosition,
				minEvent, maxEvent));

		items[i++] = new KMenuItem(new SortButton(I18n.get().i18n(
				I18n.SORT_TITLE_ALT_KEY, I18n.SORT_TITLE_ALT_VALUE),
				CatalogService.Sort.TITLE_ALT, minEvent+e++,
				panelPosition, minEvent, maxEvent));

		items[i++] = new KMenuItem(new SortButton(I18n.get().i18n(
				I18n.SORT_AUTHOR_KEY, I18n.SORT_AUTHOR_VALUE),
				CatalogService.Sort.AUTHOR, minEvent+e++, panelPosition,
				minEvent, maxEvent));

		items[i++] = new KMenuItem(new SortButton(I18n.get().i18n(
				I18n.SORT_RECENT_KEY, I18n.SORT_RECENT_VALUE),
				CatalogService.Sort.RECENT, minEvent+e++, panelPosition,
				minEvent, maxEvent));

		items[i++] = new KMenuItem(new SortButton(I18n.get().i18n(
				I18n.SORT_PUBLICATION_KEY, I18n.SORT_PUBLICATION_VALUE),
				CatalogService.Sort.PUBLICATION, minEvent+e++,
				panelPosition, minEvent, maxEvent));
		return i;
	}

	public static VirtualMenu[] getCurrentInstances(VirtualMenu parent) {
		if (Settings.get().isSortIndependently()) {
			VirtualMenu[] items = new VirtualMenu[2];
			items[0] = new SortOptionsMenu(parent, PanelPosition.UPPER);
			items[1] = new SortOptionsMenu(parent, PanelPosition.LOWER);
			return items;
		} else {
			return new VirtualMenu[] {new SortOptionsMenu(parent, PanelPosition.BOTH)};
		}
	}
}
