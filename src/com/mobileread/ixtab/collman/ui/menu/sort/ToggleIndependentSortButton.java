package com.mobileread.ixtab.collman.ui.menu.sort;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JCheckBox;

import com.mobileread.ixtab.collman.Event;
import com.mobileread.ixtab.collman.Settings;
import com.mobileread.ixtab.collman.actions.ToggleIndependentSortAction;
import com.mobileread.ixtab.collman.ui.menu.VirtualMenu;

public class ToggleIndependentSortButton extends JCheckBox implements ActionListener {
	private static final long serialVersionUID = 1L;
	
	private final VirtualMenu menuWhenToggled;

	public ToggleIndependentSortButton(VirtualMenu menuWhenToggled) {
		super(new ToggleIndependentSortAction());
		setSelected(Settings.get().isSortIndependently());
		Event.addListener(this);
		this.menuWhenToggled = menuWhenToggled;
	}
	
	public void actionPerformed(ActionEvent event) {
		Event e = (Event) event;
		if (e.getID() == Event.SORT_INDEPENDENTLY_OFF) {
			setAndSwitchMenu(false);
		} else if (e.getID() == Event.SORT_INDEPENDENTLY_ON) {
			setAndSwitchMenu(true);
		}
	}
	
	private void setAndSwitchMenu(boolean state) {
		setSelected(state);
		menuWhenToggled.show();
	}
}

