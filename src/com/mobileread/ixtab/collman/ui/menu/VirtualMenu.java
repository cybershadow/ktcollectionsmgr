package com.mobileread.ixtab.collman.ui.menu;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.Action;

import com.amazon.kindle.kindlet.ui.KMenuItem;

public abstract class VirtualMenu {
	public abstract String getLabel();

	protected abstract KMenuItem[] getItems();

	protected final VirtualMenu parent;

	protected VirtualMenu(VirtualMenu parent) {
		this.parent = parent;
	}

	protected KMenuItem getParentItem() {
		return createItem("\u25C0 " + this.getLabel(), parent);
	}

	protected KMenuItem wrap(VirtualMenu menu) {
		return createItem(menu.getLabel() + " \u25B6", menu);
	}

	private KMenuItem createItem(String label, final VirtualMenu menu) {
		Action action = new AbstractAction(label) {
			private static final long serialVersionUID = 1L;

			public void actionPerformed(ActionEvent e) {
				KindletMenu.getInstance().setPending(menu);
			}

		};
		return new KMenuItem(action);
	}

	public void show() {
		KindletMenu.getInstance().show(this);
	}
}
