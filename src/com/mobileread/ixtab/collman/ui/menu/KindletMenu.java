package com.mobileread.ixtab.collman.ui.menu;

import javax.swing.SwingUtilities;

import com.amazon.kindle.kindlet.KindletContext;
import com.amazon.kindle.kindlet.ui.KMenu;
import com.amazon.kindle.kindlet.ui.KMenuEvent;
import com.amazon.kindle.kindlet.ui.KMenuItem;
import com.amazon.kindle.kindlet.ui.KMenuListener;

public class KindletMenu extends KMenu implements KMenuListener {
	private static KindletMenu instance = null;
	
	public static KindletMenu getInstance() {
		return instance;
	}
	
	public static KindletMenu instantiate(KindletContext context, VirtualMenu main) {
		instance = new KindletMenu();
		context.setMenu(instance);
		instance.setActive(main, false);
		return instance;
	}

	private VirtualMenu root;
	private VirtualMenu active;
	private VirtualMenu pending;
	
	private KindletMenu() {
	}

	private synchronized void setActive(final VirtualMenu active, boolean display) {
		setActive(active, false, display);
	}
	
	private synchronized void setActive(final VirtualMenu active, boolean force, boolean display) {
		if (force || this.active != active) {
			this.active = active;
			if (root == null) {
				root = active;
				addMenuListener(this);
			}
			removeAll();
			
			KMenuItem[] items = active.getItems();
			for (int i=0; i < items.length; ++i) {
				add(items[i]);
			}
		}
		if (display) {
			setVisible(true);
			// just let it know that the active menu has changed
			menuShown(null);
		}
	}
	
	public void setPending(VirtualMenu menu) {
		pending = menu;
	}
	
	public synchronized void menuHidden(KMenuEvent event) {
		if (pending != null) {
			setActive(pending, true);
			pending = null;
		} else {
			setActive(root, false);
		}
	}

	public  void menuShown(KMenuEvent event) {
		// does nothing, but must be implemented.
	}

	public void show(final VirtualMenu active) {
		SwingUtilities.invokeLater(new Runnable() {

			public void run() {
				setPending(active);
				setVisible(false);
				setActive(active, true, true);
			}
			
		});
	}

}
