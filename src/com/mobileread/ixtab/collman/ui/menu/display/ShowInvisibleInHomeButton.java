package com.mobileread.ixtab.collman.ui.menu.display;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JCheckBox;

import com.mobileread.ixtab.collman.Event;
import com.mobileread.ixtab.collman.Settings;
import com.mobileread.ixtab.collman.actions.ToggleShowInvisibleItemsAction;

public class ShowInvisibleInHomeButton extends JCheckBox implements ActionListener {

	private static final long serialVersionUID = 1L;

	public ShowInvisibleInHomeButton() {
		super(new ToggleShowInvisibleItemsAction());
		setSelected(!Settings.get().isHideInvisibleInHome());
		Event.addListener(this);
	}

	public void actionPerformed(ActionEvent event) {
		Event e = (Event) event;
		if (e.getID() == Event.INVISIBLE_SHOW) {
			setSelected(true);
		} else if (e.getID() == Event.INVISIBLE_HIDE) {
			setSelected(false);
		}
	}
}
