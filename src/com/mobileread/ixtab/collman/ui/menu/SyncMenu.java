package com.mobileread.ixtab.collman.ui.menu;

import java.util.List;

import com.amazon.kindle.kindlet.ui.KMenuItem;
import com.mobileread.ixtab.collman.I18n;
import com.mobileread.ixtab.collman.actions.DeleteAllCollectionsAction;
import com.mobileread.ixtab.collman.actions.FileSystemImporterAction;
import com.mobileread.ixtab.collman.actions.LegacyExporterAction;
import com.mobileread.ixtab.collman.actions.LegacyImporterAction;

public class SyncMenu extends VirtualMenu {

	private final KMenuItem[] items;
	public SyncMenu(VirtualMenu parent) {
		super(parent);
		items = new KMenuItem[5];
		int i=0;
		items[i++] = getParentItem();
		items[i++] = new KMenuItem(new LegacyImporterAction());
		items[i++] = new KMenuItem(new LegacyExporterAction());
		items[i++] = new KMenuItem(new FileSystemImporterAction());
		items[i++] = new KMenuItem(new DeleteAllCollectionsAction());
	}

	public String getLabel() {
		return I18n.get().i18n(I18n.MENU_SYNC_KEY, I18n.MENU_SYNC_VALUE);
	}

	protected void getItemsInto(List items) {
	}

	protected KMenuItem[] getItems() {
		return items;
	}
}
