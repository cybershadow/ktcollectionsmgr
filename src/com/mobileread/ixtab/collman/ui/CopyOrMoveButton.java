package com.mobileread.ixtab.collman.ui;

import javax.swing.JButton;

import com.mobileread.ixtab.collman.actions.CopyOrMoveAction;

public class CopyOrMoveButton extends JButton {
	private static final long serialVersionUID = 1L;

	public CopyOrMoveButton(int sourcePosition, boolean move) {
		super(new CopyOrMoveAction(sourcePosition, move));
		setText(sourcePosition+" "+move);
	}
}
