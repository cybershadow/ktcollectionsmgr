package com.mobileread.ixtab.collman.ui;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import com.mobileread.ixtab.collman.Clipboard;
import com.mobileread.ixtab.collman.Event;
import com.mobileread.ixtab.collman.PanelPosition;
import com.mobileread.ixtab.collman.actions.DeleteCollectionsAction;
import com.mobileread.ixtab.collman.actions.CreateOrRenameCollectionAction;
import com.mobileread.ixtab.collman.actions.ToggleEntryVisibilityAction;
import com.mobileread.ixtab.collman.actions.ToggleLowerPanelAction;
import com.mobileread.ixtab.collman.catalog.Collection;
import com.mobileread.ixtab.collman.catalog.Entry;
import com.mobileread.ixtab.collman.icons.Icons;

public class ActionsPanel extends JPanel implements ActionListener {
	private static final long serialVersionUID = 1L;

	private final JButton ghostButton = new JButton(new ToggleEntryVisibilityAction());
	private final JButton layoutButton = new JButton(new ToggleLowerPanelAction());
	private final JButton copyDownButton = new CopyOrMoveButton(PanelPosition.UPPER, false);
	private final JButton moveDownButton = new CopyOrMoveButton(PanelPosition.UPPER, true);
	private final JButton copyUpButton = new CopyOrMoveButton(PanelPosition.LOWER, false);
	private final JButton moveUpButton = new CopyOrMoveButton(PanelPosition.LOWER, true);
	private final JButton createOrRenameButton = new JButton(new CreateOrRenameCollectionAction());
	private final JButton deleteButton = new JButton(new DeleteCollectionsAction());
	
	private boolean copyButtonsEnabled = true;
	
	public ActionsPanel(JPanel upperPanel, JPanel lowerPanel) {
		Event.addListener(this);
		setBorder(Borders.WHITE5);
		setLayout(new GridLayout(1, 7, 2, 0));
		
		add(withIcon(Icons.GHOST, ghostButton));
		add(withIcon(Icons.COPY_DOWN, copyDownButton));
		add(withIcon(Icons.MOVE_DOWN, moveDownButton));
		add(withIcon(Icons.COPY_UP, copyUpButton));
		add(withIcon(Icons.MOVE_UP, moveUpButton));
		
		add(withIcon(Icons.RENAME, createOrRenameButton));
		add(withIcon(Icons.TRASH, deleteButton));
		
		
		
		((ToggleLowerPanelAction)layoutButton.getAction()).setButton(layoutButton);
		((CreateOrRenameCollectionAction)createOrRenameButton.getAction()).setButton(createOrRenameButton);
		add(layoutButton);
	}

	private JButton withIcon(String icon, JButton button) {
		button.setText("");
		button.setIcon(Icons.load(icon));
		button.setDisabledIcon(Icons.load("g_"+icon));
		return button;
	}

	public void actionPerformed(ActionEvent e) {
		if (e instanceof Event) {
			handleInternalEvent((Event)e);
		}
	}

	private void handleInternalEvent(Event e) {
		switch (e.getID()) {
		case Event.CLIPBOARD_CHANGED:
		case Event.STARTUP:
			updateButtonState();
			break;
		case Event.LOWER_PANEL_DISABLE:
			copyButtonsEnabled = false;
			updateButtonState();
			break;
		case Event.LOWER_PANEL_ENABLE:
			copyButtonsEnabled = true;
			updateButtonState();
			break;
		default:
		}
	}

	private void updateButtonState() {
		SwingUtilities.invokeLater(new Runnable() {

			public void run() {
				int upper = Clipboard.get().countItems(PanelPosition.UPPER);
				int lower = Clipboard.get().countItems(PanelPosition.LOWER);
				ghostButton.setEnabled(upper + lower != 0);
				copyDownButton.setEnabled(copyButtonsEnabled && upper != 0);
				moveDownButton.setEnabled(copyButtonsEnabled && upper != 0);
				copyUpButton.setEnabled(copyButtonsEnabled && lower != 0);
				moveUpButton.setEnabled(copyButtonsEnabled && lower != 0);
				setDeleteButtonState();
			}

			private void setDeleteButtonState() {
				Entry[] selected = Clipboard.get().getItems(PanelPosition.BOTH);
				boolean enable = selected.length > 0;
				for (int i=0; i < selected.length; ++i) {
					if (!(selected[i] instanceof Collection)) {
						enable = false;
					}
				}
				deleteButton.setEnabled(enable);
			}
			
		});
	}
}
