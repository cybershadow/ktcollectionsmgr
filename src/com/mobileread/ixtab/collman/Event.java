package com.mobileread.ixtab.collman;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.ref.WeakReference;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class Event extends ActionEvent {
	private static final long serialVersionUID = 1L;
	
	public static final int VIEW_ENTRY_CLICKED = 1;
	public static final int PATH_ENTRY_CLICKED = 2;
	public static final int VIEW_ENTRY_SELECTED = 3;
	public static final int VIEW_ENTRY_UNSELECTED = 4;
	public static final int VIEW_RESET = 5;
	public static final int CLIPBOARD_CHANGED = 6;
	public static final int ENTRY_CHANGED = 7;
	public static final int LOWER_PANEL_ENABLE = 8;
	public static final int LOWER_PANEL_DISABLE = 9;
	public static final int STARTUP = 10;
	public static final int COLLECTION_DELETED = 11;
	public static final int SEARCH = 12;
	public static final int RELOAD = 13;
	public static final int VIEW_CHANGED = 14;
	public static final int COMMAND_LEFT = 15;
	public static final int COMMAND_RIGHT = 16;
	public static final int DISABLE_LEFT = 17;
	public static final int DISABLE_RIGHT = 18;
	public static final int ENABLE_LEFT = 19;
	public static final int ENABLE_RIGHT = 20;
	public static final int INVISIBLE_SHOW = 21;
	public static final int INVISIBLE_HIDE = 22;
	public static final int DISPLAY_COLLECTIONS_ONLY = 23;
	public static final int DISPLAY_ALL = 24;
	public static final int RELOADED = 25;
	public static final int SETTINGS_MODE_PERSISTENT = 26;
	public static final int SETTINGS_MODE_EPHEMERAL = 27;
	public static final int TOGGLE_ALL_SELECTIONS = 28;
	public static final int FILTER_LOWER_ONLY_ON = 29;
	public static final int FILTER_LOWER_ONLY_OFF = 30;
	public static final int SHOW_UNFILED_ONLY_ON = 31;
	public static final int SHOW_UNFILED_ONLY_OFF = 32;
	public static final int SORT_CHANGED = 33;
	public static final int SORT_INDEPENDENTLY_ON = 34;
	public static final int SORT_INDEPENDENTLY_OFF = 35;
	
	/* ATTENTION --- Here be dragons:
	 * The SORT_ID_{UPPER|LOWER}_{MIN|MAX} constants aren't actual
	 * events, but event identifiers for the sort menu "buttons".
	 * It's important to keep the sort events contiguous and
	 * within these ranges. In addition, the ordering must be preserved
	 * (or adjusted accordingly in the sortOptionsMenu class)
	 */
	public static final int SORT_ID_UPPER_MIN = 36;
	public static final int SORT_TITLE = 36;
	public static final int SORT_TITLE_ALT = 37;
	public static final int SORT_AUTHOR = 38;
	public static final int SORT_RECENT = 39;
	public static final int SORT_PUBLICATION = 40;
	public static final int SORT_ID_UPPER_MAX = 40;
	
	public static final int SORT_ID_LOWER_MIN = 41;
	public static final int SORT_TITLE_LOWER = 41;
	public static final int SORT_TITLE_ALT_LOWER = 42;
	public static final int SORT_AUTHOR_LOWER = 43;
	public static final int SORT_RECENT_LOWER = 44;
	public static final int SORT_PUBLICATION_LOWER = 45;
	public static final int SORT_ID_LOWER_MAX = 45;
	public static final int SET_PATH = 46;
	
	public static final String COMMAND = Event.class.getName();
	
	private static final List listeners = new LinkedList();








	
	private int position;
	
	public static void addListener(ActionListener listener) {
		synchronized (listeners) {
			listeners.add(new WeakReference(listener));
		}
	}
	
	public static boolean isInternalEvent(ActionEvent e) {
		return e.getActionCommand().equals(COMMAND);
	}
	
	public static void post(int type, Object source, int position) {
		final Event e = new Event(type, source, position);
		new Thread() {

			public void run() {
				synchronized (listeners) {
					Iterator it = listeners.iterator();
					while (it.hasNext()) {
						WeakReference weakListener = (WeakReference) it.next();
						ActionListener listener = (ActionListener) weakListener.get();
						if (listener == null) {
							it.remove();
						} else {
							listener.actionPerformed(e);
						}
					}
				}
			}
		}.start();
	}
	private Event(int type, Object source, int position) {
		super(source, type, COMMAND);
		this.position = position;
	}
	
	public boolean appliesTo(int position) {
		return (this.position & position) != 0;
	}
	
	public int getPosition() {
		return position;
	}
	
	
}
