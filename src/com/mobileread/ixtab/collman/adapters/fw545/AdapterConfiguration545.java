package com.mobileread.ixtab.collman.adapters.fw545;

import com.amazon.kindle.kindlet.KindletContext;
import com.mobileread.ixtab.collman.CollectionsManager;
import com.mobileread.ixtab.collman.adapters.AdapterConfiguration;
import com.mobileread.ixtab.collman.adapters.CatalogAdapter;
import com.mobileread.ixtab.collman.adapters.CatalogService;
import com.mobileread.ixtab.collman.adapters.PredicateFactoryAdapter;

public class AdapterConfiguration545 extends AdapterConfiguration {

	public AdapterConfiguration545() {

	};

	public PredicateFactoryAdapter getPredicateFactoryAdapter() {
		return new PredicateFactoryAdapter545();
	}

	public CatalogAdapter getCatalogAdapter() {
		return new CatalogAdapter545();
	}

	public CatalogService getCatalogService() {
		return new CatalogService545();
	}

	public CollectionsManager getCollectionManager(KindletContext context) {
		return new CollectionManager545(context);
	}

}
