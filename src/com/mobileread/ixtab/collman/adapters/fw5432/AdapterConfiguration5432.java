package com.mobileread.ixtab.collman.adapters.fw5432;

import com.amazon.kindle.kindlet.KindletContext;
import com.mobileread.ixtab.collman.CollectionsManager;
import com.mobileread.ixtab.collman.adapters.AdapterConfiguration;
import com.mobileread.ixtab.collman.adapters.CatalogAdapter;
import com.mobileread.ixtab.collman.adapters.CatalogService;
import com.mobileread.ixtab.collman.adapters.PredicateFactoryAdapter;

public class AdapterConfiguration5432 extends AdapterConfiguration {

	public AdapterConfiguration5432() {

	};

	public PredicateFactoryAdapter getPredicateFactoryAdapter() {
		return new PredicateFactoryAdapter5432();
	}

	public CatalogAdapter getCatalogAdapter() {
		return new CatalogAdapter5432();
	}

	public CatalogService getCatalogService() {
		return new CatalogService5432();
	}

	public CollectionsManager getCollectionManager(KindletContext context) {
		return new CollectionManager5432(context);
	}

}
