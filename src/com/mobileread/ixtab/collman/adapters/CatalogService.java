package com.mobileread.ixtab.collman.adapters;

import com.amazon.kindle.content.catalog.CatalogEntry;
import com.amazon.kindle.content.catalog.MutableCollection;
import com.amazon.kindle.content.catalog.MutableEntry;
import com.mobileread.ixtab.collman.PanelPosition;
import com.mobileread.ixtab.collman.Settings;

public abstract class CatalogService {

	public static final CatalogService INSTANCE = AdapterConfiguration
			.getInstance().getCatalogService();

	public static class QueryResultDepth {
		private final String type;
		public static final QueryResultDepth FAST = new QueryResultDepth("FAST");
		public static final QueryResultDepth FULL = new QueryResultDepth("FULL");

		private QueryResultDepth(String type) {
			this.type = type;
		}

		public String toString() {
			return type;
		}

	}

	public abstract MutableEntry createMutableEntry(Object uuid);

	public abstract MutableCollection createMutableCollection(Object uuid);

	public abstract Object createNewUUID();

	public abstract CatalogTransaction openTransaction();

	public abstract CatalogEntry[] find(int panel, Predicate predicate, int offset,
			int maxResults, int[] totalCountPointer, QueryResultDepth depth);
	
	protected abstract Object createCollationCriteria(String text, boolean flag);
	protected abstract Object castToCollationCriteriaArray(Object[] criteria);

	
	public static class Sort {
		// we do not use 0 here; 0 means "uninitialized/unknown"
		public static final int TITLE = 1;
		public static final int TITLE_ALT = 2;
		public static final int AUTHOR = 3;
		public static final int RECENT = 4;
		public static final int PUBLICATION = 5;
		
		private static int[] current = new int[] {Settings.get().getDisplayOrder(PanelPosition.UPPER),Settings.get().getDisplayOrder(PanelPosition.LOWER)};
		private static int[] cached = new int[2];
		private static Object[] cachedCriteria = new Object[2];
		
		public static void set(int newCollation, int panelPosition) {
			int index = (panelPosition == PanelPosition.LOWER ? 1 : 0);
			current[index] = newCollation;
		}
		
		public static Object getAsCriteria(CatalogService svc, int panelPosition) {
			int actualPosition = PanelPosition.UPPER;
			if (panelPosition == PanelPosition.LOWER && Settings.get().isSortIndependently()) {
				actualPosition = PanelPosition.LOWER;
			}
			int index = (actualPosition == PanelPosition.LOWER ? 1 : 0);
				
			if (cached[index] != current[index] || cachedCriteria[index] == null) {
				cached[index] = current[index];
				cachedCriteria[index] = createCriteria(svc, index);
			}
			return cachedCriteria[index];
		}
		
		private static Object createCriteria(CatalogService s, int index) {
			switch (cached[index]) {
			case TITLE_ALT:
		        return t(s, new Object[] {c(s,"titles[0].collation"), c(s,"credits[0].name.collation"), c(s,"publicationDate", false)});
			case AUTHOR:
				return t(s, new Object[] {c(s,"credits[0].name.collation"), c(s,"titles[0].collation")});
			case RECENT:
				return t(s, new Object[] {c(s,"lastAccess", false), c(s,"titles[0].collation")});
			case PUBLICATION:
				return t(s, new Object[] {c(s,"publicationDate", false), c(s,"titles[0].collation")});
			default:
		        return t(s, new Object[] {c(s,"titles[0].display"), c(s,"credits[0].name.collation"), c(s,"publicationDate", false)});
			}
		}
		
		private static Object c(CatalogService service, String text) {
			return c(service,text, true);
		}
		
		private static Object c(CatalogService service, String text, boolean flag) {
			return service.createCollationCriteria(text, flag);
		}
		
		private static Object t(CatalogService service, Object[] criteria) {
			return service.castToCollationCriteriaArray(criteria);
		}


	}
}
