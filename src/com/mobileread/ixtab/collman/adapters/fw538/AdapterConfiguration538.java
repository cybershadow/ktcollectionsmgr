package com.mobileread.ixtab.collman.adapters.fw538;

import com.amazon.kindle.kindlet.KindletContext;
import com.mobileread.ixtab.collman.CollectionsManager;
import com.mobileread.ixtab.collman.adapters.AdapterConfiguration;
import com.mobileread.ixtab.collman.adapters.CatalogAdapter;
import com.mobileread.ixtab.collman.adapters.CatalogService;
import com.mobileread.ixtab.collman.adapters.PredicateFactoryAdapter;

public class AdapterConfiguration538 extends AdapterConfiguration {

	public AdapterConfiguration538() {
		
	};
	
	public PredicateFactoryAdapter getPredicateFactoryAdapter() {
		return new PredicateFactoryAdapter538();
	}

	public CatalogAdapter getCatalogAdapter() {
		return new CatalogAdapter538();
	}

	public CatalogService getCatalogService() {
		return new CatalogService538();
	}

	public CollectionsManager getCollectionManager(KindletContext context) {
		return new CollectionManager538(context);
	}

}
