package com.mobileread.ixtab.collman.adapters.fw536;

import com.amazon.kindle.kindlet.KindletContext;
import com.mobileread.ixtab.collman.CollectionsManager;
import com.mobileread.ixtab.collman.adapters.AdapterConfiguration;
import com.mobileread.ixtab.collman.adapters.CatalogAdapter;
import com.mobileread.ixtab.collman.adapters.CatalogService;
import com.mobileread.ixtab.collman.adapters.PredicateFactoryAdapter;

public class AdapterConfiguration536 extends AdapterConfiguration {

	public AdapterConfiguration536() {
		
	};
	
	public PredicateFactoryAdapter getPredicateFactoryAdapter() {
		return new PredicateFactoryAdapter536();
	}

	public CatalogAdapter getCatalogAdapter() {
		return new CatalogAdapter536();
	}

	public CatalogService getCatalogService() {
		return new CatalogService536();
	}

	public CollectionsManager getCollectionManager(KindletContext context) {
		return new CollectionManager536(context);
	}

}
