package com.mobileread.ixtab.collman.adapters.fw520;

import com.amazon.kindle.booklet.BookletContext;
import com.amazon.kindle.kindlet.KindletContext;
import com.mobileread.ixtab.collman.CollectionsManager;

public class CollectionManager520 extends CollectionsManager {

	private static final long serialVersionUID = 1L;

	public CollectionManager520(KindletContext context) {
		super(context);
	}

	protected BookletContext getBookletContext() {
			return null;
	}

	protected void modifyToolbar() {
	}

	public void onStop() {
	}
	
	protected boolean isSearchSupported() {
		return false;
	}

}
