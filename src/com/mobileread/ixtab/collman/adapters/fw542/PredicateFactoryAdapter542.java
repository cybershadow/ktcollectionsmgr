package com.mobileread.ixtab.collman.adapters.fw542;

import com.amazon.kindle.content.catalog.G;
import com.amazon.kindle.content.catalog.h;
import com.mobileread.ixtab.collman.adapters.Predicate;
import com.mobileread.ixtab.collman.adapters.PredicateFactoryAdapter;

public class PredicateFactoryAdapter542 implements PredicateFactoryAdapter {

	private Predicate wrap(Object wrapped) {
		return new Predicate(wrapped);
	}

	private h[] explode(Predicate[] in) {
		if (in == null) {
			return null;
		}
		h[] out = new h[in.length];
		for (int i = 0; i < in.length; ++i) {
			out[i] = (h) in[i].delegate;
		}
		return out;
	}

	/*
	 * NOTE: Line numbers indicate where in the jadclipse-generated file the
	 * method starts. This isn't really useful *here*, but it will be useful for
	 * adapting future firmwares, which will inevitably have a different
	 * obfuscation scheme yet again. Consider these approximate, as that may
	 * also depend on the jadclipse setup etc. But these are the numbers on my
	 * system.
	 * 
	 * Oh, did I already say "FUCK YOU, Lab126"?
	 */

	// Line 19
	public Predicate and(Predicate[] predicates) {
		return wrap(G.zdc(explode(predicates)));
	}

	// 24
	public Predicate or(Predicate[] predicates) {
		return wrap(G.Lgc(explode(predicates)));
	}

	// 29
	public Predicate not(Predicate pred) {
		return wrap(G.PFc((h) pred.delegate));
	}

	// 34
	public Predicate notNull(String key) {
		return wrap(G.zec(key));
	}

	// 47
	public Predicate isTrue(String what) {
		return wrap(G.Jgc(what));
	}

	// 81
	public Predicate equals(String key, String value) {
		return wrap(G.vNb(key, value));
	}

	// 129
	public Predicate inList(String key, Object membersArray) {
		return wrap(G.SDc(key, CatalogAdapter542
				.asUUIDArray((Object[]) membersArray)));
	}

	// 265
	public Predicate greater(String key, long value, boolean inclusive) {
		return wrap(G.GEc(key, value, inclusive));
	}

	// 313
	public Predicate startsWith(String key, String value) {
		return wrap(G.mec(key, value));
	}

}
