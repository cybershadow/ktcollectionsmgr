package com.mobileread.ixtab.collman.adapters.fw542;

import com.amazon.kindle.kindlet.KindletContext;
import com.mobileread.ixtab.collman.CollectionsManager;
import com.mobileread.ixtab.collman.adapters.AdapterConfiguration;
import com.mobileread.ixtab.collman.adapters.CatalogAdapter;
import com.mobileread.ixtab.collman.adapters.CatalogService;
import com.mobileread.ixtab.collman.adapters.PredicateFactoryAdapter;

public class AdapterConfiguration542 extends AdapterConfiguration {

	public AdapterConfiguration542() {
		
	};
	
	public PredicateFactoryAdapter getPredicateFactoryAdapter() {
		return new PredicateFactoryAdapter542();
	}

	public CatalogAdapter getCatalogAdapter() {
		return new CatalogAdapter542();
	}

	public CatalogService getCatalogService() {
		return new CatalogService542();
	}

	public CollectionsManager getCollectionManager(KindletContext context) {
		return new CollectionManager542(context);
	}

}
