package com.mobileread.ixtab.collman.adapters;

public class PredicateFactory {
	
	private static final PredicateFactoryAdapter adapter = AdapterConfiguration.getInstance().getPredicateFactoryAdapter();

	public static Predicate startsWith(String key, String value) {
		return adapter.startsWith(key, value);
	}

	public static Predicate and(Predicate[] predicates) {
		return adapter.and(predicates);
	}

	public static Predicate isTrue(String what) {
		return adapter.isTrue(what);
	}

	public static Predicate not(Predicate pred) {
		return adapter.not(pred);
	}

	public static Predicate equals(String key, String value) {
		return adapter.equals(key, value);
	}

	public static Predicate or(Predicate[] predicates) {
		return adapter.or(predicates);
	}

	public static Predicate notNull(String key) {
		return adapter.notNull(key);
	}

	public static Predicate inList(String key, Object[] members) {
		return adapter.inList(key, members);
	}

	public static Predicate greater(String key, long value, boolean inclusive) {
		return adapter.greater(key, value, inclusive);
	}

}
