package com.mobileread.ixtab.collman.adapters.fw540;

import com.amazon.kindle.content.catalog.g;
import com.amazon.kindle.content.catalog.i;
import com.mobileread.ixtab.collman.adapters.Predicate;
import com.mobileread.ixtab.collman.adapters.PredicateFactoryAdapter;

public class PredicateFactoryAdapter540 implements PredicateFactoryAdapter {

	private Predicate wrap(Object wrapped) {
		return new Predicate(wrapped);
	}

	private i[] explode(Predicate[] in) {
		if (in == null) {
			return null;
		}
		i[] out = new i[in.length];
		for (int i = 0; i < in.length; ++i) {
			out[i] = (i) in[i].delegate;
		}
		return out;
	}

	/*
	 * NOTE: Line numbers indicate where in the jadclipse-generated file the
	 * method starts. This isn't really useful *here*, but it will be useful for
	 * adapting future firmwares, which will inevitably have a different
	 * obfuscation scheme yet again. Consider these approximate, as that may
	 * also depend on the jadclipse setup etc. But these are the numbers on my
	 * system.
	 * 
	 * Oh, did I already say "FUCK YOU, Lab126"?
	 */

	// Line 19
	public Predicate and(Predicate[] predicates) {
		return wrap(g.xTB(explode(predicates)));
	}

	// 24
	public Predicate or(Predicate[] predicates) {
		return wrap(g.mSB(explode(predicates)));
	}

	// 29
	public Predicate not(Predicate pred) {
		return wrap(g.XsB((i) pred.delegate));
	}

	// 34
	public Predicate notNull(String key) {
		return wrap(g.utB(key));
	}

	// 47
	public Predicate isTrue(String what) {
		return wrap(g.TSB(what));
	}

	// 81
	public Predicate equals(String key, String value) {
		return wrap(g.Heb(key, value));
	}

	// 129
	public Predicate inList(String key, Object membersArray) {
		return wrap(g.DsB(key, CatalogAdapter540
				.asUUIDArray((Object[]) membersArray)));
	}

	// 265
	public Predicate greater(String key, long value, boolean inclusive) {
		return wrap(g.vtB(key, value, inclusive));
	}

	// 313
	public Predicate startsWith(String key, String value) {
		return wrap(g.wTB(key, value));
	}

}
