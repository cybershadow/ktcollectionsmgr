package com.mobileread.ixtab.collman.adapters.fw534;

import com.amazon.kindle.kindlet.KindletContext;
import com.mobileread.ixtab.collman.CollectionsManager;
import com.mobileread.ixtab.collman.adapters.AdapterConfiguration;
import com.mobileread.ixtab.collman.adapters.CatalogAdapter;
import com.mobileread.ixtab.collman.adapters.CatalogService;
import com.mobileread.ixtab.collman.adapters.PredicateFactoryAdapter;

public class AdapterConfiguration534 extends AdapterConfiguration {

	public AdapterConfiguration534() {
		
	};
	
	public PredicateFactoryAdapter getPredicateFactoryAdapter() {
		return new PredicateFactoryAdapter534();
	}

	public CatalogAdapter getCatalogAdapter() {
		return new CatalogAdapter534();
	}

	public CatalogService getCatalogService() {
		return new CatalogService534();
	}

	public CollectionsManager getCollectionManager(KindletContext context) {
		return new CollectionManager534(context);
	}

}
