package com.mobileread.ixtab.collman.adapters;

import com.amazon.kindle.kindlet.KindletContext;
import com.mobileread.ixtab.collman.CollectionsManager;
import com.mobileread.ixtab.collman.firmware.FirmwareVersion;
import com.mobileread.ixtab.collman.firmware.UnsupportedFirmwareException;

public abstract class AdapterConfiguration {
	private static AdapterConfiguration instance;

	public static synchronized AdapterConfiguration getInstance() {
		if (instance == null) {
			String id = FirmwareVersion.getIdentifier();
			instance = instantiate(id);
		}
		return instance;
	}

	private static AdapterConfiguration instantiate(String identifier) {
		if (identifier == null) {
			throw new UnsupportedFirmwareException(null);
		}
		String firmware = stripDots(identifier.trim());
		// special cases
		if (firmware.equals("510")) {
			firmware = "512";
		}
		else if (firmware.equals("5321")) {
			firmware = "532";
		} else if (firmware.equals("5371")) {
			firmware = "537";
		} else if (firmware.equals("5372")) {
			firmware = "537";
		} else if (firmware.equals("5381")) {
			firmware = "538";
		} else if (firmware.equals("5421")) {
			firmware = "542";
		} else if (firmware.equals("5431")) {
			firmware = "5432";
		} else if (firmware.equals("544")) {
			firmware = "5442";
		} else if (firmware.equals("5441")) {
			firmware = "5442";
		} else if (firmware.equals("5451")) {
			firmware = "545";
		}

		String className = constructClassnameFrom(firmware);
		try {
			return instantiateClass(className);
		} catch (ClassNotFoundException nope) {
			throw new UnsupportedFirmwareException(identifier);
		}
	}

	private static String constructClassnameFrom(String firmware) {
		StringBuffer result = new StringBuffer("com.mobileread.ixtab.collman.adapters.fw");
		result.append(firmware);
		result.append(".AdapterConfiguration");
		result.append(firmware);
		return result.toString();
	}

	private static String stripDots(String withDots) {
		StringBuffer sb = new StringBuffer();
		char[] chars = withDots.toCharArray();
		for (int i=0; i < chars.length; ++i) {
			char c = chars[i];
			if (c >= '0' && c <= '9') {
				sb.append(c);
			}
		}
		return sb.toString();
	}

	private static AdapterConfiguration instantiateClass(String classname) throws ClassNotFoundException {
		try {
			Class clazz = Class.forName(classname);
			return (AdapterConfiguration) clazz.newInstance();
		} catch (ClassNotFoundException e) {
			throw e;
		} catch (Throwable t) {
			throw new RuntimeException("FATAL ERROR: Unable to instantiate "
					+ classname, t);
		}
	}

	public abstract CollectionsManager getCollectionManager(
			KindletContext context);

	public abstract PredicateFactoryAdapter getPredicateFactoryAdapter();

	public abstract CatalogService getCatalogService();

	public abstract CatalogAdapter getCatalogAdapter();

}
