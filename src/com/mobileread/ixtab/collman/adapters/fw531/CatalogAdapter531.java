package com.mobileread.ixtab.collman.adapters.fw531;

import java.util.Date;

import com.amazon.ebook.util.a.g;
import com.amazon.ebook.util.text.m;
import com.amazon.kindle.content.catalog.f;
import com.amazon.kindle.content.catalog.CatalogEntry;
import com.amazon.kindle.content.catalog.CatalogEntryCollection;
import com.amazon.kindle.content.catalog.CatalogItem;
import com.amazon.kindle.content.catalog.MutableCollection;
import com.amazon.kindle.content.catalog.MutableEntry;
import com.mobileread.ixtab.collman.adapters.CatalogAdapter;

public class CatalogAdapter531 extends CatalogAdapter {

	public void setMembers(MutableCollection mutable, Object[] uuids) {
		g[] ids = asUUIDArray(uuids);
		mutable.Wj(ids);
	}

	protected static g[] asUUIDArray(Object[] in) {
		if (in == null) {
			return null;
		}
		g[] out = new g[in.length];
		for (int i=0; i < in.length; ++i) {
			out[i] = (g) in[i];
		}
		return out;
	}

	public void addMember(MutableCollection mutable, Object uuid) {
		mutable.cH((g) uuid);
	}

	public void setIsVisibleInRoot(MutableCollection mutable,
			boolean visible) {
		mutable.DE(visible);
	}

	public void setTitle(MutableCollection mutable, String title) {
		mutable.Wf(new m[] {new m(title)});
	}

	public Object getUUID(CatalogEntry entry) {
		return entry.AG();
	}

	public String getLocation(CatalogEntry entry) {
		return entry.qg();
	}

	public String getTitle(CatalogEntry entry) {
		m[] titles = entry.Ef();
		return (titles == null || titles.length < 1) ? "" : titles[0].toString();
	}

	public String getCDEKey(CatalogItem item) {
		return item.af();
	}

	public String getCDEType(CatalogItem item) {
		return item.CF();
	}

	public Date getLastAccessDate(CatalogEntryCollection collection) {
		return collection.Kg();
	}

	public boolean isVisibleInHome(CatalogEntry entry) {
		return entry.Nf();
	}

	public void setIsVisibleInHome(MutableEntry mutable, boolean visible) {
		mutable.DE(visible);
	}

	public int countParents(CatalogEntry entry) {
		return entry.Be();
	}

	public Object[] getParents(CatalogEntry entry) {
		return entry.zD();
	}

	public String getFirstCreditAsJSON(CatalogEntry entry) {
		f[] credits = entry.IF();
		if (credits == null || credits.length < 1) {
			return null;
		}
		return credits[0].toJSONString();
	}

	public Object[] getMembers(CatalogEntryCollection collection) {
		return collection.rH();
	}

	public int countMembers(CatalogEntryCollection collection) {
		return collection.Ch();
	}

	public void setTitle(MutableEntry mutable, String title) {
		mutable.Wf(new m[] {new m(title)});
	}

}
