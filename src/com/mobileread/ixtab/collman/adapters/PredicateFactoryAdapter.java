package com.mobileread.ixtab.collman.adapters;

public interface PredicateFactoryAdapter {

	Predicate startsWith(String key, String value);

	Predicate and(Predicate[] predicates);

	Predicate isTrue(String what);

	Predicate not(Predicate pred);

	Predicate equals(String key, String value);

	Predicate or(Predicate[] predicates);

	Predicate notNull(String key);

	Predicate inList(String key, Object membersArray);

	Predicate greater(String key, long value, boolean inclusive);

}
