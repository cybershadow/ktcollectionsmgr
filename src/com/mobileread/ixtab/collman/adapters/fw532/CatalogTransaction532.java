package com.mobileread.ixtab.collman.adapters.fw532;

import com.amazon.ebook.util.a.g;
import com.amazon.kindle.content.catalog.MutableEntry;
import com.mobileread.ixtab.collman.adapters.CatalogTransaction;

public class CatalogTransaction532 extends CatalogTransaction {

	private final com.amazon.kindle.content.catalog.CatalogTransaction delegate;
	public CatalogTransaction532(
			com.amazon.kindle.content.catalog.CatalogTransaction delegate) {
		this.delegate = delegate;
	}

	public void deleteEntry(Object uuid) {
		delegate.ZG((g) uuid);
	}

	public boolean commitSync() {
		return delegate.Di().zbB();
	}

	public void addEntry(MutableEntry c) {
		delegate.Vh(c);
	}

	public void updateEntry(MutableEntry entry) {
		delegate.vj(entry);
	}

}
