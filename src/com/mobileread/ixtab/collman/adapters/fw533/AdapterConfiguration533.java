package com.mobileread.ixtab.collman.adapters.fw533;

import com.amazon.kindle.kindlet.KindletContext;
import com.mobileread.ixtab.collman.CollectionsManager;
import com.mobileread.ixtab.collman.adapters.AdapterConfiguration;
import com.mobileread.ixtab.collman.adapters.CatalogAdapter;
import com.mobileread.ixtab.collman.adapters.CatalogService;
import com.mobileread.ixtab.collman.adapters.PredicateFactoryAdapter;

public class AdapterConfiguration533 extends AdapterConfiguration {

	public AdapterConfiguration533() {
		
	};
	
	public PredicateFactoryAdapter getPredicateFactoryAdapter() {
		return new PredicateFactoryAdapter533();
	}

	public CatalogAdapter getCatalogAdapter() {
		return new CatalogAdapter533();
	}

	public CatalogService getCatalogService() {
		return new CatalogService533();
	}

	public CollectionsManager getCollectionManager(KindletContext context) {
		return new CollectionManager533(context);
	}

}
