package com.mobileread.ixtab.collman.adapters.fw512;

import java.util.Map;

import com.amazon.ebook.util.lang.UUID;
import com.amazon.kindle.content.catalog.CatalogEntry;
import com.amazon.kindle.content.catalog.CatalogService.QueryResults;
import com.amazon.kindle.content.catalog.CollationCriteria;
import com.amazon.kindle.content.catalog.MutableCollection;
import com.amazon.kindle.content.catalog.MutableEntry;
import com.amazon.kindle.restricted.runtime.Framework;
import com.mobileread.ixtab.collman.adapters.CatalogService;
import com.mobileread.ixtab.collman.adapters.CatalogTransaction;
import com.mobileread.ixtab.collman.adapters.Predicate;

public class CatalogService512 extends CatalogService {

	private final com.amazon.kindle.content.catalog.CatalogService delegate = (com.amazon.kindle.content.catalog.CatalogService) Framework
			.getService(com.amazon.kindle.content.catalog.CatalogService.class);

	public MutableEntry createMutableEntry(Object uuid) {
		return delegate.createMutableEntry((UUID) uuid);
	}

	public MutableCollection createMutableCollection(Object uuid) {
		return delegate.createMutableCollection((UUID) uuid);
	}

	public Object createNewUUID() {
		return new UUID();
	}

	public CatalogTransaction openTransaction() {
		return new CatalogTransaction512(delegate.openTransaction());
	}

	public CatalogEntry[] find(int context, Predicate predicate, int offset, int maxResults,
			final int[] totalCountPointer, QueryResultDepth depth) {
		// easy way to simulate a pointer using an array
		final CatalogEntry[][] raw = new CatalogEntry[][] { new CatalogEntry[0] };
		synchronized (raw) {
			if (delegate
					.find((com.amazon.kindle.content.catalog.Predicate) predicate.delegate,
							getCollation(context), maxResults, offset, new QueryResults() {

								public void queryComplete(boolean flag, int i,
										int j, CatalogEntry[] acatalogentry,
										Map map) {
									if (totalCountPointer != null) {
										totalCountPointer[0] = i;
									}
									synchronized (raw) {
										raw[0] = acatalogentry;
										raw.notify();
									}
								}
							}, convert(depth))) {
				try {
					raw.wait();
				} catch (InterruptedException e) {
				}
				return raw[0];
			} else {
				return new CatalogEntry[0];
			}
		}
	}

	private com.amazon.kindle.content.catalog.CatalogService.QueryResultDepth convert(
			QueryResultDepth depth) {
		return depth == QueryResultDepth.FAST ? com.amazon.kindle.content.catalog.CatalogService.QueryResultDepth.FAST
				: com.amazon.kindle.content.catalog.CatalogService.QueryResultDepth.FULL;
	}

	private CollationCriteria[] getCollation(int context) {
		return (CollationCriteria[]) Sort.getAsCriteria(this, context);
	}

	protected Object castToCollationCriteriaArray(Object[] criteria) {
		CollationCriteria[] result = new CollationCriteria[criteria.length];
		for (int i=0; i < criteria.length; ++i) {
			result[i] = (CollationCriteria) criteria[i];
		}
		return result;
	}

	protected Object createCollationCriteria(String text, boolean flag) {
		return new CollationCriteria(text, flag);
	}

}
