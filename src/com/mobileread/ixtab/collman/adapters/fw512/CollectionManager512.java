package com.mobileread.ixtab.collman.adapters.fw512;

import java.lang.reflect.Field;

import com.amazon.kindle.booklet.BookletContext;
import com.amazon.kindle.booklet.ChromeException;
import com.amazon.kindle.booklet.ChromeSearchBarButton;
import com.amazon.kindle.booklet.ChromeSearchBarRequest;
import com.amazon.kindle.kindlet.KindletContext;
import com.mobileread.ixtab.collman.CollectionsManager;

public class CollectionManager512 extends CollectionsManager {

	private static final long serialVersionUID = 1L;

	public CollectionManager512(KindletContext context) {
		super(context);
	}

	protected BookletContext getBookletContext() {
		try {
			com.amazon.kindle.kindlet.internal.ui.x x = (com.amazon.kindle.kindlet.internal.ui.x) context.getService(com.amazon.kindle.kindlet.ui.Toolbar.class);
			Field f = x.getClass().getDeclaredField("e");
			f.setAccessible(true);

			f = x.getClass().getDeclaredField("g");
			f.setAccessible(true);
			return (BookletContext) f.get(x);
		} catch (Throwable t) {
			return null;
		}
	}

	protected void modifyToolbar() {
		if (chromeImplementation == null || bookletContext == null) {
			return;
		}

		boolean registerSearch = true;
		try {
			chromeImplementation.D(bookletContext, "default", SearchHandler
					.getInstance().getChromeSearchProvider());
		} catch (ChromeException e) {
			registerSearch = false;
		}
		if (registerSearch) {
			SearchHandler.getInstance().register(bookletContext);
		}

		try {
			ChromeSearchBarRequest sbr = new ChromeSearchBarRequest(
					"com.lab126.booklet.kindlet");
			sbr.setProfile("default",
					new ChromeSearchBarButton[] {
							new ChromeSearchBarButton("forward", "invisible",
									"system"),
							new ChromeSearchBarButton("back", "invisible",
									"system"), });
			chromeImplementation.setSearchBar(bookletContext, sbr, true);
		} catch (ChromeException e) {
			// this is actually known to throw an exception, yet it still
			// provides the wanted result. Go figure.
		}
	}

	public void onStop() {
		SearchHandler.getInstance().unregister(bookletContext);
	}
	
	protected boolean isSearchSupported() {
		return true;
	}
}
