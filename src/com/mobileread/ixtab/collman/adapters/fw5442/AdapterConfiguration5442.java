package com.mobileread.ixtab.collman.adapters.fw5442;

import com.amazon.kindle.kindlet.KindletContext;
import com.mobileread.ixtab.collman.CollectionsManager;
import com.mobileread.ixtab.collman.adapters.AdapterConfiguration;
import com.mobileread.ixtab.collman.adapters.CatalogAdapter;
import com.mobileread.ixtab.collman.adapters.CatalogService;
import com.mobileread.ixtab.collman.adapters.PredicateFactoryAdapter;

public class AdapterConfiguration5442 extends AdapterConfiguration {

	public AdapterConfiguration5442() {

	};

	public PredicateFactoryAdapter getPredicateFactoryAdapter() {
		return new PredicateFactoryAdapter5442();
	}

	public CatalogAdapter getCatalogAdapter() {
		return new CatalogAdapter5442();
	}

	public CatalogService getCatalogService() {
		return new CatalogService5442();
	}

	public CollectionsManager getCollectionManager(KindletContext context) {
		return new CollectionManager5442(context);
	}

}
