package com.mobileread.ixtab.collman.adapters.fw5442;

import com.amazon.ebook.util.a.h;
import com.amazon.kindle.content.catalog.MutableEntry;
import com.mobileread.ixtab.collman.adapters.CatalogTransaction;

public class CatalogTransaction5442 extends CatalogTransaction {

	private final com.amazon.kindle.content.catalog.CatalogTransaction delegate;
	public CatalogTransaction5442(
			com.amazon.kindle.content.catalog.CatalogTransaction delegate) {
		this.delegate = delegate;
	}

	public void deleteEntry(Object uuid) {
		delegate.UL((h) uuid);
	}

	public boolean commitSync() {
		return delegate.Ok().iGc();
	}

	public void addEntry(MutableEntry c) {
		delegate.QJ(c);
	}

	public void updateEntry(MutableEntry entry) {
		delegate.dm(entry);
	}

}
