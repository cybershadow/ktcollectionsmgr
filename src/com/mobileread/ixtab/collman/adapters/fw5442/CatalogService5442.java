package com.mobileread.ixtab.collman.adapters.fw5442;

import java.util.Map;

import com.amazon.ebook.util.a.h;
import com.amazon.kindle.content.catalog.CatalogEntry;
import com.amazon.kindle.content.catalog.MutableCollection;
import com.amazon.kindle.content.catalog.MutableEntry;
import com.amazon.kindle.content.catalog.C;
import com.amazon.kindle.content.catalog.CatalogService.QueryResults;
import com.amazon.kindle.restricted.runtime.Framework;
import com.mobileread.ixtab.collman.adapters.CatalogService;
import com.mobileread.ixtab.collman.adapters.CatalogTransaction;
import com.mobileread.ixtab.collman.adapters.Predicate;

public class CatalogService5442 extends CatalogService {

	private final com.amazon.kindle.content.catalog.CatalogService delegate = (com.amazon.kindle.content.catalog.CatalogService) Framework
			.getService(com.amazon.kindle.content.catalog.CatalogService.class);

	public MutableEntry createMutableEntry(Object uuid) {
		return delegate.ik((h) uuid);
	}

	public MutableCollection createMutableCollection(Object uuid) {
		return delegate.qK((h) uuid);
	}

	public Object createNewUUID() {
		return new h();
	}

	public CatalogTransaction openTransaction() {
		return new CatalogTransaction5442(delegate.hk());
	}

	public CatalogEntry[] find(int context, Predicate predicate, int offset, int maxResults,
			final int[] totalCountPointer, QueryResultDepth depth) {
		// easy way to simulate a pointer using an array
		final CatalogEntry[][] raw = new CatalogEntry[][] { new CatalogEntry[0] };
		synchronized (raw) {
			if (delegate
					.mJ((com.amazon.kindle.content.catalog.h) predicate.delegate,
							getCollation(context), maxResults, offset, new QueryResults() {

								public void ZB(boolean flag, int i,
										int j, CatalogEntry[] acatalogentry,
										Map map) {
									if (totalCountPointer != null) {
										totalCountPointer[0] = i;
									}
									synchronized (raw) {
										raw[0] = acatalogentry;
										raw.notify();
									}
								}
							}, convert(depth))) {
				try {
					raw.wait();
				} catch (InterruptedException e) {
				}
				return raw[0];
			} else {
				return new CatalogEntry[0];
			}
		}
	}

	private com.amazon.kindle.content.catalog.b convert(
			QueryResultDepth depth) {
		return depth == QueryResultDepth.FAST ? com.amazon.kindle.content.catalog.b.l
				: com.amazon.kindle.content.catalog.b.e;
	}

	private C[] getCollation(int context) {
		return (C[]) Sort.getAsCriteria(this, context);
	}

	protected Object castToCollationCriteriaArray(Object[] criteria) {
		C[] result = new C[criteria.length];
		for (int i=0; i < criteria.length; ++i) {
			result[i] = (C) criteria[i];
		}
		return result;
	}

	protected Object createCollationCriteria(String text, boolean flag) {
		return new C(text, flag);
	}
}
