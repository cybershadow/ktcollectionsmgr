package com.mobileread.ixtab.collman.adapters.fw535;

import com.amazon.ebook.util.a.g;
import com.amazon.kindle.content.catalog.MutableEntry;
import com.mobileread.ixtab.collman.adapters.CatalogTransaction;

public class CatalogTransaction535 extends CatalogTransaction {

	private final com.amazon.kindle.content.catalog.CatalogTransaction delegate;
	public CatalogTransaction535(
			com.amazon.kindle.content.catalog.CatalogTransaction delegate) {
		this.delegate = delegate;
	}

	public void deleteEntry(Object uuid) {
		delegate.ZG((g) uuid);
	}

	public boolean commitSync() {
		return delegate.Di().HBB();
	}

	public void addEntry(MutableEntry c) {
		delegate.Vh(c);
	}

	public void updateEntry(MutableEntry entry) {
		delegate.vj(entry);
	}

}
