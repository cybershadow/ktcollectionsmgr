package com.mobileread.ixtab.collman.adapters.fw535;

import com.amazon.kindle.kindlet.KindletContext;
import com.mobileread.ixtab.collman.CollectionsManager;
import com.mobileread.ixtab.collman.adapters.AdapterConfiguration;
import com.mobileread.ixtab.collman.adapters.CatalogAdapter;
import com.mobileread.ixtab.collman.adapters.CatalogService;
import com.mobileread.ixtab.collman.adapters.PredicateFactoryAdapter;

public class AdapterConfiguration535 extends AdapterConfiguration {

	public AdapterConfiguration535() {
		
	};
	
	public PredicateFactoryAdapter getPredicateFactoryAdapter() {
		return new PredicateFactoryAdapter535();
	}

	public CatalogAdapter getCatalogAdapter() {
		return new CatalogAdapter535();
	}

	public CatalogService getCatalogService() {
		return new CatalogService535();
	}

	public CollectionsManager getCollectionManager(KindletContext context) {
		return new CollectionManager535(context);
	}

}
