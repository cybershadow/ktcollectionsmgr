package com.mobileread.ixtab.collman.adapters.fw535;

import com.amazon.kindle.content.catalog.b;
import com.amazon.kindle.content.catalog.g;
import com.mobileread.ixtab.collman.adapters.Predicate;
import com.mobileread.ixtab.collman.adapters.PredicateFactoryAdapter;

public class PredicateFactoryAdapter535 implements PredicateFactoryAdapter {
	
	private Predicate wrap(Object wrapped) {
		return new Predicate(wrapped);
	}

	private g[] explode(
			Predicate[] in) {
		if (in == null) {
			return null;
		}
		g[] out = new g[in.length];
		for (int i=0; i < in.length; ++i) {
			out[i] = (g) in[i].delegate;
		}
		return out;
	}

	public Predicate and(Predicate[] predicates) {
		return wrap(b.cBB(explode(predicates)));
	}

	public Predicate or(Predicate[] predicates) {
		return wrap(b.LCB(explode(predicates)));
	}

	public Predicate not(Predicate pred) {
		return wrap(b.hBB((g) pred.delegate));
	}

	public Predicate notNull(String key) {
		return wrap(b.HdB(key));
	}

	public Predicate isTrue(String what) {
		return wrap(b.QBB(what));
	}

	public Predicate equals(String key, String value) {
		return wrap(b.qVA(key, value));
	}

	public Predicate inList(String key, Object membersArray) {
		return wrap(b.aBB(key, CatalogAdapter535.asUUIDArray((Object[]) membersArray)));
	}

	public Predicate greater(String key, long value, boolean inclusive) {
		return wrap(b.ydB(key, value, inclusive));
	}

	public Predicate startsWith(String key, String value) {
		return wrap(b.SdB(key, value));
	}

}
