package com.mobileread.ixtab.collman.actions;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;

import com.mobileread.ixtab.collman.Clipboard;
import com.mobileread.ixtab.collman.Event;
import com.mobileread.ixtab.collman.Model;
import com.mobileread.ixtab.collman.PanelPosition;

public class SyncPanelsAction extends AbstractAction {

	private static final long serialVersionUID = 1L;
	public static final int DIRECTION_UPPER_TO_LOWER = 1;
	public static final int DIRECTION_LOWER_TO_UPPER = 2;

	private final int direction;

	public SyncPanelsAction(int direction) {
		this.direction = direction;
	}

	public void perform() {
		this.actionPerformed(null);
	}

	/**
	 * Note: parameter is ignored, so callers can safely pass in null instead of
	 * an actual ActionEvent instance. In fact, use #perform() for a simpler way.
	 */
	public void actionPerformed(ActionEvent e) {
		//Status.set("SYNC: "+direction, 5000);
		int sourcePanel = PanelPosition.UPPER;
		int targetPanel = PanelPosition.LOWER;

		if (direction == DIRECTION_LOWER_TO_UPPER) {
			sourcePanel = PanelPosition.LOWER;
			targetPanel = PanelPosition.UPPER;
		}

		int selected = Clipboard.get().countItems(PanelPosition.BOTH);
		if (selected != 0) {
			/* We only allow panel synchronization if no entries are selected
			 * in either panel. This is on purpose: CM has buttons to
			 * copy/move entries, and implementing Android-like swipes would probably
			 * just add more confusion ("is this copying, or moving, or synchronizing?").
			 * 
			 * This may be revised in the future though if a more intuitive way is found.
			 */
			return;
		}

		Model sourceModel = Model.get(sourcePanel);
		Event.post(Event.SET_PATH, sourceModel.getPathUuids(), targetPanel);
	}

}
