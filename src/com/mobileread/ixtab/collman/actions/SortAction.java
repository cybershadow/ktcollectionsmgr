package com.mobileread.ixtab.collman.actions;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;

import com.mobileread.ixtab.collman.Event;
import com.mobileread.ixtab.collman.PanelPosition;

public class SortAction extends AbstractAction {

	private static final long serialVersionUID = 1L;
	private final int eventToPost;

	public SortAction(String label, int eventToPost) {
		super(label);
		this.eventToPost = eventToPost;
	}

	public void actionPerformed(ActionEvent e) {
		Event.post(eventToPost, this, PanelPosition.BOTH);
	}

}
