package com.mobileread.ixtab.collman.actions;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;

import com.mobileread.ixtab.collman.Event;
import com.mobileread.ixtab.collman.I18n;
import com.mobileread.ixtab.collman.PanelPosition;
import com.mobileread.ixtab.collman.Settings;

public class ToggleIndependentSortAction extends AbstractAction {

	private static final long serialVersionUID = 1L;

	public ToggleIndependentSortAction() {
		super(I18n.get().i18n(I18n.SORT_INDEPENDENTLY_KEY, I18n.SORT_INDEPENDENTLY_VALUE));
	}

	public void actionPerformed(ActionEvent e) {
		Event.post(Settings.get().isSortIndependently() ? Event.SORT_INDEPENDENTLY_OFF : Event.SORT_INDEPENDENTLY_ON, this, PanelPosition.BOTH);
	}

}
