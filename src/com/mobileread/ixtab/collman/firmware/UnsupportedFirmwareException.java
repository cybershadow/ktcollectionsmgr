package com.mobileread.ixtab.collman.firmware;

public class UnsupportedFirmwareException extends RuntimeException {
	private static final long serialVersionUID = 1L;
	
	private final String version;
	
	public UnsupportedFirmwareException(String version) {
		super();
		this.version = version == null ?  null : version.trim();
	}

	public String getVersion() {
		return version == null ? "UNKNOWN" : version;
	}

}
